module github.com/arnaucube/go-snark

require (
	github.com/arnaucube/cryptofun v0.0.0-20181124004321-9b11ae8280bd
	github.com/stretchr/testify v1.2.2
)
